
# Fibonacci Test

Calcula el n-ésimo término de la sucesión de Fibonacci dado un número.


### Requisitos

+ PHP >= 7.2
+ Apache 2.4.29


### Para Ejecutar

Una vez colocada la carpeta del proyecto en el servidor, desde el navegardor ingresar la URL y el nombre del archivo **index.php**. Por ejemplo: ```http://localhost/index.php```

Si desea realizar una prueba mostrando la serie puede agregar **../index.php?test=10** en la URL. El **10** es el número a evaluar.

