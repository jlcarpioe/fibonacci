<?php

// numero
$number = '';

// secuencia fibonacci
$secuencia = '';



/**
 * Calcula el n-esimo termino
 * 
 * @param  int $n numero para la sucesionde Fibonacci
 * @return int n-esimo termino
 * 
 * @see  https://stackoverflow.com/questions/15600041/php-fibonacci-sequence#answer-27190248
 * 
 */
function fibonacci($n) {
	return round(pow((sqrt(5)+1)/2, $n) / sqrt(5));
}



// verifica si viene dato
if ($_POST['number'] && is_numeric($_POST['number'])) {

	$number = $_POST['number'];

	// termino
	$ntermino = fibonacci($number);	
}


// prueba para verificar
if ($_GET['test'])  {

	$number = ($_GET['test'] > 1) ? intval($_GET['test']) : 7;
	$ntermino = 0;

	$n1 = 0;
	$n2 = 1;

	// contador por la cantidad de numeros
	for ($counter = 0; $counter < $number; $counter++){   
		$secuencia .= "$n1 ";

		$n3 = $n1 + $n2;

		$n1 = $n2;
		$n2 = $n3;
	}

	$ntermino = $n1;
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Fibonnaci</title>
</head>
<body>

	<form method="post" action="index.php">
		<input type="text" name="number" value="<?= $number ?>" />
		<button type="submit">Obtener n-&eacute;simo t&eacute;rmino</button>
	</form>

	<br />

	<?php 
	if ($number) {
		echo "<div>El n-&eacute;simo t&eacute;rmino de $number es $ntermino</div>";
	}

	if ($secuencia) {
		echo "<div>Secuencia Fibonnaci: [ $secuencia ]</div>";
	}
	?>
</body>